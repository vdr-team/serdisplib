/*
 *************************************************************************
 *
 * serdisp_connect_net.c
 * routines for accessing displays over a network-based protocol
 *
 *************************************************************************
 *
 * heavily based on serdisp_connect_srv.c and serdisp_srvtools.c (//MAF)
 *
 * copyright (C) 2011-2018   wolfgang astleitner
 * email     mrwastl@users.sourceforge.net
 *
 *************************************************************************
 * This program is free software; you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by   
 * the Free Software Foundation; either version 2 of the License, or (at  
 * your option) any later version.                                        
 *                                                                        
 * This program is distributed in the hope that it will be useful, but    
 * WITHOUT ANY WARRANTY; without even the implied warranty of             
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      
 * General Public License for more details.                               
 *                                                                        
 * You should have received a copy of the GNU General Public License      
 * along with this program; if not, write to the Free Software            
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA              
 * 02111-1307, USA.  Or, point your browser to                            
 * http://www.gnu.org/copyleft/gpl.html                                   
 *************************************************************************
 */

/*#define DEBUG_SIGNALS 1*/

#include "../config.h"

#include <syslog.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
//#include <ctype.h>        /* isspace */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>  /* TCP_NODELAY */

#include "serdisplib/serdisp_connect.h"
//#include "serdisplib/serdisp_srvtools.h"
#include "serdisplib/serdisp_connect_net.h"
#include "serdisplib/serdisp_tools.h"
#include "serdisplib/serdisp_messages.h"

#include "serdisplib/serdisp_fctptr.h"


/* *********************************
   serdisp_CONN_t* SDCONNnet_open(sdcdev)
   *********************************
   opens an remote device for a serdisp lcd
   *********************************
   sdcdev   ... device identifier. format:

        devid = 'NET:' srvname [':'port]

      srvname - Server name or address
      port    - Server port (if not given: default port)

               examples:
                "NET:192.168.1.1"
                "NET:localhost"
                "NET:myhost.mydomain:12532"

   *********************************
   returns a serdisp connect descriptor or (serdisp_CONN_t*)0 if operation was unsuccessful

*/
serdisp_CONN_t* SDCONNnet_open( const char sdcdev[] ) {
  serdisp_CONN_t*    sdcd = 0;
  char*              hostname;
  char*              idx;
  int                port = SD_NET_DEFPORT;
  void*              extra;
  struct hostent *   hostptr;
  struct sockaddr_in*srvaddr;
  struct sockaddr_in*cliaddr;
  int                fd;
  int                rc;
  uint16_t           protocol = SDPROTO_TCP;

  /* parse device string */

  /* TCP or UDP */
  if (strncasecmp(sdcdev, "UDP:", 4) == 0) {
    protocol = SDPROTO_UDP;
  }

  idx = strchr( &sdcdev[4], ':' );
  if( idx ) {
    hostname = sdtools_malloc(idx - &sdcdev[4] + 1);
    if (hostname) {
      long tempport;
      sdtools_strncpy(hostname, &sdcdev[4], idx - &sdcdev[4]);
      rc = sdtools_strtol(idx+1, '\0', 10, &tempport);
      port = (int)tempport;
      /* verify if port containes a valid number */
      if ((port <= 0) || (rc != 1)) {
        sd_error(SERDISP_ENXIO, "%s(): invalid protocol (bad port)", __func__);
        free( hostname );
        return NULL;
      }
    } else {
      sd_error(SERDISP_EMALLOC, "%s(): cannot allocate memory for hostname", __func__);
      return NULL;
    }
  } else {
    hostname = sdtools_malloc(strlen(&sdcdev[4])+1);
    if (hostname) {
      sdtools_strncpy(hostname, &sdcdev[4], strlen(&sdcdev[4]) );
      port = SD_NET_DEFPORT;
    } else {
      sd_error(SERDISP_EMALLOC, "%s(): cannot allocate memory for hostname", __func__);
      return NULL;
    }
  }

  extra = (void*)sdtools_malloc( 2 * sizeof(struct sockaddr_in));
  if( !extra ) {
    sd_error(SERDISP_EMALLOC, "%s(): unable to allocate memory for sdcd->extra", __func__);
    return NULL;
  }
  memset( extra, 0, 2 * sizeof(struct sockaddr_in) );

  cliaddr = extra;
  srvaddr = extra + sizeof(struct sockaddr_in);

  /* connect to server */

  /* get server address */
  /*memset( &srvaddr, 0, sizeof(srvaddr) );*/
  sd_debug( 2, "%s(): resolving host '%s'", __func__, hostname );
  hostptr = fp_gethostbyname( hostname );
  if( !hostptr ) {
    sd_error(SERDISP_ENXIO, "%s(): cannot get host by name", __func__);
    return NULL;
  }
  bcopy( hostptr->h_addr, (char*)&(srvaddr->sin_addr), hostptr->h_length );
  srvaddr->sin_family = AF_INET;
  srvaddr->sin_port   = fp_htons( port );
  /*free(hostptr);*/

  /* create socket and connect to server */
  sd_debug( 2, "%s(): connecting to %s:%d", __func__, hostname, port );
  if (protocol == SDPROTO_UDP) {
    fd = fp_socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
  } else {
    int flag = 1;
    fd = fp_socket( AF_INET, SOCK_STREAM, 0 );
    if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (char*) &flag, sizeof(int)) < 0) {
      perror("setsockopt");
    }
  }
  if( fd<0 ) {
    sd_error(SERDISP_ENXIO, "%s(): could not open socket", __func__);
    return NULL;
  }

  if (protocol == SDPROTO_UDP) {
    cliaddr->sin_family = AF_INET;
    cliaddr->sin_addr.s_addr = htonl(INADDR_ANY);
    cliaddr->sin_port = htons(port);
    if (bind(fd, (struct sockaddr*) cliaddr, sizeof(struct sockaddr_in) ) < 0) {
      perror("bind");
      free (extra);
      close(fd);
      return NULL;
    }
    sd_debug( 1, "%s(): bound to %s:%d", __func__, hostname, port );
  } else {
    if( fp_connect(fd, (struct sockaddr*) srvaddr, sizeof(struct sockaddr_in)) )  {
      sd_error(SERDISP_ENXIO, "%s(): could not connect", __func__);
      perror("connect");
      free (extra);
      close(fd);
      return NULL;
    }
    sd_debug( 1, "%s(): connected to %s:%d", __func__, hostname, port );
  }


  /* set up connection decriptor */
  sdcd = (serdisp_CONN_t*)sdtools_malloc( sizeof(serdisp_CONN_t) );
  if( !sdcd ) {
    sd_error(SERDISP_EMALLOC, "%s(): unable to allocate memory for sdcd", __func__);
    return NULL;
  }
  memset( sdcd, 0, sizeof(serdisp_CONN_t) );
  sdcd->sdcdev       = (char*)sdcdev;
  sdcd->conntype     = SDCT_INET;
  sdcd->hardwaretype = SDHWT_INET;
  /*fprintf(stderr, "assigning fd: %d\n", fd);*/
  sdcd->protocol     = protocol;
  sdcd->fd           = fd;
  sdcd->extra        = extra;

  return sdcd;
}


/* *********************************
   long SDCONNnet_read(sdcd, flags)
   *********************************
   read a byte from the serdisp connect device
   *********************************
   sdcd   ... serdisp connect descriptor
   flags  ... which bytes are to read
   *********************************
   returns a data read from the serdisp connect device
   =================================================

*/
uint32_t SDCONNnet_read(serdisp_CONN_t* sdcd, byte flags) {
  char buf[1];
  int rc = 0;

  if (sdcd->conntype & SDCT_RS232) {
    rc = (int)read(sdcd->fd, buf, 1);
  } else {
    if (sdcd->protocol != SDPROTO_UDP) {
      rc = (int)recv(sdcd->fd, buf, 1, 0);
    } else {
      struct sockaddr_in srcaddr;
      struct sockaddr_in * srvaddr;
      int valid = 0;
      srvaddr = (struct sockaddr_in *)(sdcd->extra + sizeof(struct sockaddr_in));
      socklen_t addrlen = sizeof(srcaddr);
      do {
        rc = (int)recvfrom(sdcd->fd, buf, 1, 0, (struct sockaddr *) &srcaddr, &addrlen);
        valid = (srcaddr.sin_addr.s_addr == srvaddr->sin_addr.s_addr) && (srcaddr.sin_port == srvaddr->sin_port);
      } while (! valid);
    }
  }

  return (rc > 0) ? (uint32_t)buf[0] : 0; /* SDCONNnet_readstream() should be used instead of SDCONN_read() ! */
}


/* *********************************
   int SDCONNnet_readstream(sdcd, buf, count)
   *********************************
   read a stream from the serdisp connect device into a buffer
   *********************************
   sdcd   ... serdisp connect descriptor
   buf    ... buffer for stream
   count  ... read up to 'count' bytes info the buffer (note: count <= sizeof(buf) !!!)
   *********************************
   returns the number of bytes read.
      0 ... indicates end of stream
     -1 ... error when reading stream
   =================================================

*/
int SDCONNnet_readstream(serdisp_CONN_t* sdcd, byte* buf, int count) {
  int rc = 0;

  if (sdcd->conntype & SDCT_RS232) {
    rc = (int)read(sdcd->fd, buf, count);
  } else {
    if (sdcd->protocol != SDPROTO_UDP) {
      //rc = (int)recv(sdcd->fd, buf, count, 0);
      rc = (int)read(sdcd->fd, buf, count);
    } else {
      struct sockaddr_in srcaddr;
      struct sockaddr_in * srvaddr;
      int valid = 0;
      srvaddr = (struct sockaddr_in *)(sdcd->extra + sizeof(struct sockaddr_in));
      socklen_t addrlen = sizeof(srcaddr);
fprintf(stderr, "[recvfrom len=%d: ", count);
      do {
        rc = (int)recvfrom(sdcd->fd, buf, count, 0, (struct sockaddr *) &srcaddr, &addrlen);
fprintf(stderr, "rc=%d, ", rc);
        valid = (srcaddr.sin_addr.s_addr == srvaddr->sin_addr.s_addr) && (srcaddr.sin_port == srvaddr->sin_port);
fprintf(stderr, "valid=%d, ", valid);
      } while (! valid);
fprintf(stderr, "] ");
    }
  }

  if( rc < 0 ) {
    if ( errno!=EAGAIN ) {
      sd_error(SERDISP_ERUNTIME, "%s(): could not read from device: %s (%d)", __func__, strerror(errno), errno);
    } else {
      usleep(100);
    }
  }

  return rc;
}


/* *********************************
   void SDCONNnet_close(sdcd)
   *********************************
   close the device occupied by serdisp
   *********************************
   sdcd     ... serdisp connect descriptor
   *********************************
*/
void SDCONNnet_close(serdisp_CONN_t* sdcd) {
  close( sdcd->fd );
  free( sdcd->extra );
}


